# 冒泡排序

## 背景概述

我喜欢 Go 语言，我用的最多的却还是 Fortran。

2022 年 6 月 8 日，我在 B 站看到有一个 Up 主，进行了 Python、JavaScript、Java、Go
语言代码的冒泡排序的跑分，我觉得可以用 Fortran 跑一下。

在我之前的一篇帖子 [《比较：使用Go语言与Fortran语言求解二维泊松方程》](https://zhuanlan.zhihu.com/p/501224385?) 中，
Go 语言性能出乎意料的好，作为一门新语言，背靠 Google 和开源社区、大佬云集，它是有实力的。
我喜欢 Go ，基本出于两个原因：

- 易学，类脚本语言式的现代编码风格；
- 性能强、稳定向好，开源 + 商业双驱动，我敬佩它有垃圾回收机制，性能竟如此优秀。

## Go 语言代码

Go 语言代码由 [korori-ck](https://github.com/korori-ck) 公布在 GitHub 的
[korori-ck/test](https://github.com/korori-ck/test/blob/main/demo.go) 上，
本库将 `Print` 换成了 `Println`，置于 `app/` 路径。

```sh
>> go run app/main.go
```

- 在 Mint OS (Linux) 上，Go 1.13.8 版本，I3-1005G1 CPU 上，运行时长为 45 ms；
- 在 Windows 10 上，MSYS2-UCRT-MINGW64-GO 1.18.2 上，运行时长为 58 ms。

备注：Windows 上的 Go 一般要比 Linux 上的 Go 慢一点，MSYS2 的 Go 不知道会不会更慢一丢丢？

## Fortran 代码

Fortran 语言代码可采样 `fortran-lang/fpm` 构建，也可手动编译：

```sh
>> fpm run --profile release   # fpm 构建 (默认：GFortran)
>> gfortran -Ofast -march=native app/main.f90 && ./a.out
>> fpm run --profile release --compiler ifort  # fpm 构建 (IFORT)
>> ifort -Ofast -march=native app/main.f90 && ./a.out  # IFX/IFORT 性能接近, Windows 上使用 `-QxHost` 替代 `-march=native`
```

备注：`fpm release` 命令行与 `-Ofast -march=native` 不同。

- 在 Mint OS (Linux) 上，I3-1005G1 CPU 上，运行时长：
    - GFortran 9.4.0 (fpm 构建) 版本，运行时长约为 34.265 ms；
    - GFortran 9.4.0 (纯命令行) 版本，运行时长约为 33.675 ms；
    - IFORT(IFX) 2022.2.0 (fpm 构建) 版本，运行时长约为 36.820 ms；
    - IFORT(IFX) 2022.2.0 (纯命令行) 版本，运行时长约为 31.803 ms。
- 在 Windows 10 上，GFortran 为 MSYS2-UCRT-MINGW64 环境，I5-8250U CPU 上，运行时长：
    - GFortran 12.1.0 (fpm 构建) 版本，运行时长约为 31.250 ms；
    - GFortran 12.1.0 (纯命令行) 版本，运行时长约为 31.250 ms；
    - IFORT(IFX) 2022.2.0 (fpm 构建) 版本，运行时长约为 62.500 ms；
    - IFORT(IFX) 2022.2.0 (纯命令行) 版本，运行时长约为 46.875 ms；
    - WSL (Debian OS) 上，GFortran 10.2.1 (fpm 构建) 版本，运行时长约为 34.421 ms；
    - WSL (Debian OS) 上，GFortran 10.2.1 (纯命令行) 版本，运行时长约为 33.304 ms。
    
备注 1：Fortran 的表现极不稳定，存在明显波动，以上采用较好的数据，Go 也是，
我怀疑 Fortran 这边调用动态链接库是一个问题，Go 则默认采用静态编译，毕竟
毫秒级的跑分，格局还是小了，意义不显著。

备注 2：如果你发现 Fortran 的跑分只有 0.1 ms，或者远远低于 Go，这里可能是因为编译器优化代码的原因——
编译器认为这段代码没有输出可以优化掉，你需要打印一下数组长度，以使用计算结果，这样就不会被优化掉。

## 小结

可以看到，Go 代码在这份冒泡排序上跑分接近 Fortran 了，不容小觑，当然一个例子也不能直接说 Go 性能几乎等价于 Fortran 了，仅供参考；
即使是这样，我仍相信高性能计算 (HPC) 是属于那些无垃圾回收机制的语言的领域，Rust 在这行列，
Python 在 HPC 上的应用实际上还是依赖后端的 C、C++、Fortran 代码；
拥有垃圾回收的语言，要么作为计算前端语言，为用户提供优秀的编码交互，要么应用于商业开发中，高容错性的商业 app 开发，
总不希望我们部署在服务端的应用动不动就宕机吧。
