program main

    implicit none
    real :: t1, t2
    integer, parameter :: n = 10000, inside_int = 1000001
    integer :: array(n), i

    call cpu_time(t1)
    do i = 1, n
        array(i) = inside_int - i   ! Fortran 数组索引从 1 开始
    end do

    call bubble_sort(array)
    call cpu_time(t2)
    print "(g0,a)", (t2 - t1)*1000, "ms"

contains

    !> 冒泡排序
    pure subroutine bubble_sort(unsorted)
        integer, intent(inout) :: unsorted(:)
        integer :: ik, jk, isize, itmp

        isize = size(unsorted)
        do ik = 1, isize

            do jk = ik, isize
                if (unsorted(ik) > unsorted(jk)) then
                    itmp = unsorted(ik)
                    unsorted(ik) = unsorted(jk)
                    unsorted(jk) = itmp
                end if
            end do

        end do

    end subroutine bubble_sort

end program main
